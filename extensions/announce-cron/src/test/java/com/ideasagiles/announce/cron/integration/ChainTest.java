/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ideasagiles.announce.cron.integration;

import java.io.File;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.message.GenericMessage;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests for chain element in spring integration configuration.
 */
@ContextConfiguration(locations = {
    "classpath:context.xml",
    "classpath:context-test.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ChainTest {

    @Autowired
    @Qualifier("fileChannel")
    private MessageChannel inputChannel;
    @Autowired
    @Qualifier("testLinesChannel")
    private QueueChannel testChannel;
    @Autowired
    @Qualifier("controlBusChannel")
    private MessageChannel controlBusChannel;
    private static final int MESSAGE_RECEIVE_TIMEOUT_MS = 600;
    private static final String SCHEDULE_FILE_EMPTY = "target/test-classes/com/ideasagiles/announce/cron/integration/schedule.empty.cron";
    private static final String SCHEDULE_FILE_BLANK = "target/test-classes/com/ideasagiles/announce/cron/integration/schedule.blank.cron";
    private static final String SCHEDULE_FILE_NORMAL = "target/test-classes/com/ideasagiles/announce/cron/integration/schedule.normal.cron";

    @Before
    public void setup() {
        testChannel.clear();
        //start bridge to send messages to test
        controlBusChannel.send(new GenericMessage<>("@linesChannelToTestChannelBridge.start()"));
    }

    @Test
    public void testChain_emptyFile_returnNothing() {
        File payload = new File(SCHEDULE_FILE_EMPTY);
        Message<File> message = MessageBuilder.withPayload(payload).build();
        inputChannel.send(message);
        //wait for message
        Message<?> receive = testChannel.receive(MESSAGE_RECEIVE_TIMEOUT_MS);
        assertNull("Empty lines must be filtered.", receive);
    }

    @Test
    public void testChain_fileWithCommentsAndBlankLines_returnNothing() {
        File payload = new File(SCHEDULE_FILE_BLANK);
        Message<File> message = MessageBuilder.withPayload(payload).build();
        inputChannel.send(message);
        //wait for message
        Message<?> receive = testChannel.receive(MESSAGE_RECEIVE_TIMEOUT_MS);
        assertNull("Empty lines must be filtered.", receive);
    }

    @Test
    public void testChain_normalFile_returnLines() {
        String[] expectedLines = new String[3];
        expectedLines[0] = "0 0 * * * *           | the top of every hour of every day";
        expectedLines[1] = "*/10 * * * * *        | every ten seconds";
        expectedLines[2] = "0 0 8-10 * * *        | 8, 9 and 10 o'clock of every day";

        File payload = new File(SCHEDULE_FILE_NORMAL);
        Message<File> message = MessageBuilder.withPayload(payload).build();
        inputChannel.send(message);
        for (int i = 0; i < expectedLines.length; i++) {
            Message<String> receive = (Message<String>) testChannel.receive(MESSAGE_RECEIVE_TIMEOUT_MS);
            assertNotNull(receive);
            assertEquals(expectedLines[i], receive.getPayload());
        }
        //there should be no more messages
        assertEquals("There should be no more messages.", 0, testChannel.getQueueSize());
    }
}
