/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ideasagiles.announce.cron.integration;

import com.ideasagiles.announce.cron.service.CronService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.ReplaceWithMock;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.integration.message.GenericMessage;
import org.springframework.test.annotation.DirtiesContext;
import static org.mockito.Mockito.*;

/**
 * Tests for Service Activator that schedules annoucements.
 */
@ContextConfiguration(loader = SpringockitoContextLoader.class,
        locations = {
    "classpath:context.xml",
    "classpath:context-test.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CronServiceActivatorTest {

    @Autowired
    @Qualifier("linesChannel")
    private MessageChannel inputChannel;

    @ReplaceWithMock
    @Autowired
    private CronService cronService;

    @Autowired
    @Qualifier("controlBusChannel")
    private MessageChannel controlBusChannel;

    @Before
    public void setup() {
        controlBusChannel.send(new GenericMessage<>("@cronServiceActivator.start()"));
    }

    @Test
    @DirtiesContext
    public void testServiceActivator_validCronExpression_schedulesAnnouncement() {
        String cron = "0 0 * * * * ";
        String text = " the top of every hour of every day";
        String line = cron + "|" + text;

        Message<String> message = MessageBuilder.withPayload(line).build();
        inputChannel.send(message);

        verify(cronService).scheduleAnnounce(cron, text);
    }

}
