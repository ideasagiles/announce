/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ideasagiles.announce.cron;

import java.io.File;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

/**
 *
 * @author ldeseta
 */
public class AcceptIfChangedFileListFilterTest {

    private AcceptIfChangedFileListFilter acceptIfChangedFileListFilter;

    private static final String FILE_NAME = "test.cron";

    @Before
    public void setup() {
        acceptIfChangedFileListFilter = new AcceptIfChangedFileListFilter();
        acceptIfChangedFileListFilter.setFileName(FILE_NAME);
    }

    private File createFile(String name, long lastModified) {
        File file = Mockito.mock(File.class);
        doReturn(name).when(file).getName();
        doReturn(lastModified).when(file).lastModified();

        return file;
    }

    @Test
    public void filterFiles_oneFileMatches_returnsFile() {
        File[] files = new File[3];
        files[0] = createFile("shouldNotMatch1", System.currentTimeMillis());
        files[1] = createFile(FILE_NAME, System.currentTimeMillis());
        files[2] = createFile("shouldNotMatch2", System.currentTimeMillis());

        List<File> filterFiles = acceptIfChangedFileListFilter.filterFiles(files);

        assertNotNull(filterFiles);
        assertEquals("There should be 1 file matching.", 1, filterFiles.size());
        assertEquals(files[1], filterFiles.get(0));
    }

    @Test
    public void filterFiles_noFileMatches_returnsEmptyList() {
        File[] files = new File[3];
        files[0] = createFile("shouldNotMatch1", System.currentTimeMillis());
        files[1] = createFile("shouldNotMatch2", System.currentTimeMillis());
        files[2] = createFile("shouldNotMatch3", System.currentTimeMillis());

        List<File> filterFiles = acceptIfChangedFileListFilter.filterFiles(files);

        assertNotNull(filterFiles);
        assertTrue(filterFiles.isEmpty());
    }

    @Test
    public void filterFiles_fileIsFilteredAndThenNotModified_returnsEmptyList() {
        File[] files = new File[1];
        files[0] = createFile(FILE_NAME, System.currentTimeMillis());

        List<File> filterFiles = acceptIfChangedFileListFilter.filterFiles(files);
        assertNotNull(filterFiles);
        assertEquals("There should be 1 file matching.", 1, filterFiles.size());

        filterFiles = acceptIfChangedFileListFilter.filterFiles(files);

        assertNotNull(filterFiles);
        assertTrue(filterFiles.isEmpty());
    }

    @Test
    public void filterFiles_fileIsFilteredAndThenModified_returnsFile() {
        File[] files = new File[1];
        files[0] = createFile(FILE_NAME, System.currentTimeMillis());

        List<File> filterFiles = acceptIfChangedFileListFilter.filterFiles(files);
        assertNotNull(filterFiles);
        assertEquals("There should be 1 file matching.", 1, filterFiles.size());

        doReturn(System.currentTimeMillis()).when(files[0]).lastModified();

        filterFiles = acceptIfChangedFileListFilter.filterFiles(files);
        assertNotNull(filterFiles);
        assertEquals("There should be 1 file matching.", 1, filterFiles.size());
    }

}