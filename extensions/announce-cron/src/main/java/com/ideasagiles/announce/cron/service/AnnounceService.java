/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.announce.cron.service;

/**
 * Sends an annoucement to Announce! Service.
 */
public interface AnnounceService {

    /** Sends a text to announce to the configured Announce! Service. */
    public void sendAnnouncement(String text);

}
