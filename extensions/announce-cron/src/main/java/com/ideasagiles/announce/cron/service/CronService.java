/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.announce.cron.service;

import com.ideasagiles.announce.cron.AnnounceMessageTask;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

/**
 * Creates schedules using cron-like expressions for announcing messages.
 */
public abstract class CronService {

    private static final Logger logger = LoggerFactory.getLogger(CronService.class);

    @Autowired
    private TaskScheduler scheduler;

    private List<ScheduledFuture> scheduledFutures = new ArrayList();

    /** Setups a trigger for the given cron and text. 
     * @param cron a cron-like expression.
     * @param text the text to announce by this trigger.
     */
    public void scheduleAnnounce(String cron, String text) {
        cron = cron.trim();
        text = text.trim();

        AnnounceMessageTask task = createTask();
        task.setText(text);
        ScheduledFuture future = scheduler.schedule(task, new CronTrigger(cron));
        scheduledFutures.add(future);
        logger.info("Job scheduled: " + cron + " | " + text);
    }

    /** Creates a new task for running an announce. */
    public abstract AnnounceMessageTask createTask();

    /** Cancels all running jobs. */
    public void cancelAllJobs() {
        logger.info("Cancelling {} jobs.", scheduledFutures.size());
        for (ScheduledFuture scheduledFuture : scheduledFutures) {
            scheduledFuture.cancel(true);
        }
        scheduledFutures.clear();
    }
}
