/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.announce.cron;

import com.ideasagiles.announce.cron.service.AnnounceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * A task for sending an announcement to the Announce! Server.
 */
@Component(value = "announceMessageTask")
@Scope(value = "prototype")
@Lazy(value = true)
public class AnnounceMessageTask implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(AnnounceMessageTask.class);

    @Autowired
    private AnnounceService announceService;

    private String text;

    @Override
    public void run() {
        logger.debug("Sending announcement: " + text);
        announceService.sendAnnouncement(text);
    }

    public void setText(String text) {
        this.text = text;
    }
}
