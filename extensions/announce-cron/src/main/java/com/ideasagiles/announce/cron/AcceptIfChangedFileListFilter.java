/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.announce.cron;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.springframework.integration.file.filters.FileListFilter;

/**
 * A filter that accepts a file only if it has changed since the last check.
 * The first time it checks it always returns the file with the given fileName;
 * then it only returns the file if it was modified after the last check.
 */
public class AcceptIfChangedFileListFilter implements FileListFilter<File> {

    private String fileName;
    private long lastModified = Long.MIN_VALUE;

    @Override
    public List<File> filterFiles(File[] fs) {
        List<File> filtered = new ArrayList();
        for (File file : fs) {
            if (file.getName().equals(fileName) && file.lastModified() > lastModified) {
                lastModified = file.lastModified();
                filtered.add(file);
            }
        }
        return filtered;
    }

    /** Sets the name of the filter to filter. Only files matching this name 
     * will be considered.
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
