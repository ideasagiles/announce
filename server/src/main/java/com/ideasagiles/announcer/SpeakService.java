/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.announcer;

import com.gtranslate.Audio;
import java.io.IOException;
import java.io.InputStream;
import javazoom.jl.decoder.JavaLayerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A service class to play text messages as audio.
 */
public class SpeakService {

    private static Logger logger = LoggerFactory.getLogger(SpeakService.class);
    /** An optional audio file to play before speaking an announcement. */
    private String beforeAnnounceSoundPath;

    /**
     * Plays as audio the text message, in the given language.
     * @param message a message to play as audio.
     * @param language a two-code language code (available in com.gtranslate.Language).
     */
    public void speak(String message, String language) throws IOException, JavaLayerException {
        logger.debug("Message received. Language: {}, Message: {}", language, message);

        Audio audio = Audio.getInstance();

        if (beforeAnnounceSoundPath != null) {
            InputStream beforeAnnounceSound = getClass().getClassLoader().getResourceAsStream(beforeAnnounceSoundPath);
            audio.play(beforeAnnounceSound);
        }

        InputStream sound = audio.getAudio(message, language);
        audio.play(sound);
    }

    public void setBeforeAnnounceSoundPath(String beforeAnnounceSoundPath) {
        this.beforeAnnounceSoundPath = beforeAnnounceSoundPath;
    }

}
