var announcer = {
    statusWorking: function() {
        $("#submitButton").attr("disabled", "disabled");
        $("#ajaxLoader").show();
    },
    statusAvailable: function() {
        $("#submitButton").removeAttr("disabled");
        $("#ajaxLoader").hide();
    },
    createGETUrl: function() {
        return document.baseURI + $("#announceForm").attr("action") + "?" + $("#announceForm").serialize();
    },
    updateDemoCode: function() {
        $("#code").html(announcer.createGETUrl());
    },
    speak: function() {
        announcer.statusWorking();

        $.ajax({
            url: $("#announceForm").attr("action"),
            data: $("#announceForm").serialize(),
            type: $("#announceForm").attr("method"),
            complete: function() {
                announcer.statusAvailable();
            }
        });

        return false;
    }

};

$(document).ready(function() {
    $("#ajaxLoader").hide();
    $("#announceForm").on("submit", announcer.speak);

    $("#message").on("keyup change paste cut", announcer.updateDemoCode);
    $("#language").on("keyup change paste cut", announcer.updateDemoCode);
    $("#message").trigger("change"); //force first update
});