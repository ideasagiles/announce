<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Announce! - A text-to-speech announcer that plays text messages using a HTTP API</title>
        <link href="images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>

        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="css/docs.css" rel="stylesheet" media="screen"/>
    </head>
    <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="./index.jsp">Announcer</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li class="">
                                <a href="https://bitbucket.org/ideasagiles/announce">Source code</a>
                            </li>
                            <li class="">
                                <a href="https://bitbucket.org/ideasagiles/announce/wiki/Home">Documentation</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <header class="jumbotron subhead" id="overview">
            <div class="container">
                <h1>Announce!</h1>
                <p class="lead">Make yourself heard. Say something to your team.</p>
            </div>
        </header>


        <div class="container">
            <div class="row">
                <div class="span12">
                    <section id="announcement-section">
                        <form id="announceForm" class="form-inline" action="services/announcements" method="POST">
                            <input id="message" type="text" name="text" class="input-xxlarge" placeholder="What do you want to announce?" size="80" maxlength="100" pattern=".{3,100}" title="The messsage to announce! (3 to 100 characters)" required="required"/>
                            <input id="language" type="text" name="language" class="input-mini" value="EN" placeholder="Lang?" title="What language should I speak?" size="2"/>
                            <input id="submitButton" type="submit" class="btn btn-primary" value="Announce this!">
                            &nbsp;&nbsp;<img id="ajaxLoader" src="images/ajax-loader.gif" alt="loading..." />
                        </form>
                    </section>
                    <section id="code-section">
                        <h6>HTTP GET:</h6>
                        <pre id="code"></pre>
                    </section>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container">
                <p>Code licensed under <a href="http://www.mozilla.org/MPL/2.0/" target="_blank">Mozilla Public License v2.0</a>, documentation under <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.</p>
            </div>
        </footer>


        <script src="http://code.jquery.com/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/index.js" type="text/javascript"></script>

    </body>
</html>
