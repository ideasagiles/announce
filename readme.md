# Announce!
##### A text-to-speech announcer that plays text messages with a HTTP API.

## What is Announce!?
Announce! is a text-to-speech system that plays text messages using a synthetic
voice. The voice is reproduced in the computer where Announce! is installed.
Announce! uses a HTTP API for sending messages to play. It also provides a web
console to quickly announce messages! :)

## Directories
* server : the main Announce! server.
* extensions : optional extensions that provide additional features and tools.

## License
Announce! is distributed under the Mozilla Public License, version 2.0.
The LICENSE file contains more information about the licesing of this product.
You can read more about the MPL at Mozilla Public License FAQ.